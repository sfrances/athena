/*
  Copyright (C) 2002-2025 CERN for the benefit of the ATLAS collaboration
*/

#ifndef JIVEXML_XAODPHOTONRETRIEVER_H
#define JIVEXML_XAODPHOTONRETRIEVER_H

#include <string>
#include <vector>
#include <map>

#include "JiveXML/IDataRetriever.h"
#include "AthenaBaseComps/AthAlgTool.h"
#include "xAODEgamma/PhotonContainer.h"

namespace JiveXML{

  /**
   * @class xAODPhotonRetriever
   * @brief Retrieves all @c Photon @c objects (PhotonAODCollection etc.)
   *
   *  - @b Properties
   *    - PriorityClusterCollection: First collection to be retrieved, displayed
   *      in Atlantis without switching. All other collections are
   *      also retrieved.
   *    - OtherClusterCollections
   *    - DoWriteHLT
   *    - DoWriteAllCollections
   *
   *  - @b Retrieved @b Data
   *    - Usual four-vectors: phi, eta, et etc.
   *    - Associations for clusters and tracks via ElementLink: key/index scheme
   */
  class xAODPhotonRetriever : virtual public IDataRetriever,
			      public AthAlgTool {

  public:

    /// Standard Constructor
    xAODPhotonRetriever(const std::string& type,const std::string& name,const IInterface* parent);

    virtual StatusCode retrieve(ToolHandle<IFormatTool> &FormatTool);
    /// Puts the variables into a DataMap
    const DataMap getData(const xAOD::PhotonContainer*);
    /// Gets the StoreGate keys for the desired containers
    const std::vector<std::string> getKeys();
    /// Return the name of the data type that is generated by this retriever
    virtual std::string dataTypeName() const { return m_typeName; };

  private:

    /// The data type that is generated by this retriever
    const std::string m_typeName = "Photon";

    Gaudi::Property<bool> m_doWriteHLT {this, "DoWriteHLT", false, "Write out other collections that have HLT in the name"};
    Gaudi::Property<bool> m_doWriteAllCollections {this, "DoWriteAllCollections", false, "Write out all Photon collections"};
    Gaudi::Property<std::vector<std::string>> m_otherKeys {this, "OtherPhotonCollections", {}, "Other collections to be retrieved. If DoWriteAllCollections is set to true all available Photon collections will be retrieved"};
    Gaudi::Property<std::string> m_priorityKey {this,"PriorityPhotonCollection","Photons", "Name of the priority Photon container that will be written out first"};

  };
}
#endif
